# Booklovers frontend.

Done with ReactJs

# install

Clone the project.
Go to booklovers_front:

- `cd booklovers_front`

Install the project in local:

- `docker-compose up -d`

To use the project in local you'll need to clone and do the readme instruction to the backend repository: booklovers_backend
