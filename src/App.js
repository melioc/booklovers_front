import React, { useState, useEffect } from "react";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Home from "./features/home/Home";
import Footer from "./components/footer/Footer";
import Header from "./components/header/Header";
import Login from "./features/users/login/Login";
import SignUp from "./features/users/signUp/SignUp";
import Account from "./features/users/account/Account";
import Favorites from "./features/favorites/Favorites";
import ResetPassword from "./features/users/resetPassword/ResetPassword"
import Cookie from "js-cookie";
import axiosInstance from "./conf/api.users";
import axiosFavoritesInstance from "./conf/api.favorites";
import axiosBooksInstance from "./conf/api.books";
import { AppContext } from "./libs/contextLib";
import "./assets/css/App.scss";

const App = () => {
	/* Main class to display all the app functionalities */
	const [isLoading, setIsLoading] = useState(false);
	const [error, setError] = useState(false);
	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	const [userInfo, setUserInfo] = useState({});
	const [userFav, setUserFav] = useState({});
	const [isLogged, setIsLogged] = useState(false);
	const [msgError, setMsgError] = useState("");
	const [details, setDetails] = useState(false);
	const [searchTerm, setSearchTerm] = useState("");
	const [booksResult, setBooksResult] = useState();

	const onInputChange = (e) => {
		setSearchTerm(e.target.value);
	};

	const handleClick = () => {
		setDetails(true);
	};

	const handleDetailsDisplay = () => {
		getUserFav();
		setDetails(false);
	};

	const handleReturn = () => {
		setIsLoading(false);
	};

	const fetchBooks = () => {
		// Ajax call to API using axios
		setError(false);
		try {
			// fetching books search results query
			axiosBooksInstance
				.get(`searchBooks/?search=${searchTerm}`)
				.then((resp) => {
					const result = resp.data.results;
					setBooksResult(result);
					setIsLoading(true);
				});
		} catch (error) {
			// return django error
			setError(true);
		}
	};

	// Getting the input search's value

	const getUserInfo = () => {
		const token = Cookie.get("token");
		axiosInstance
			.get("user/", {
				headers: {
					"Content-Type": "application/json",
					Authorization: `Bearer ${token}`,
				},
			})
			.then((resp) => {
				const info = resp.data;
				setUserInfo(info);
			})
			.catch((e) => {
				setError(true);
				console.log(e);
			});
	};

	const getUserFav = () => {
		const token = Cookie.get("token");
		axiosFavoritesInstance
			.get("", {
				headers: {
					Authorization: `Bearer ${token}`,
				},
			})
			.then((resp) => {
				const fav = resp.data;
				setUserFav(fav);
			})
			.catch((e) => setError(e));
	};

	useEffect(() => {
		if (isLoading) {
			fetchBooks();
		}
	}, [isLoading]);

	useEffect(() => {
		if (isLogged) {
			getUserInfo();
			getUserFav();
		}
		return setIsLoading(false);
	}, [isLogged]);

	return (
		<Router>
			<div className="App">
				<AppContext.Provider value={{ isLogged, setIsLogged }}>
					<Header
						userInfo={userInfo}
						getUserInfo={getUserInfo}
						userFav={userFav}
						getUserFav={getUserFav}
						handleReturn={handleReturn}
					/>
					<Switch>
						<Route path="/login">
							<Login
								email={email}
								setEmail={setEmail}
								password={password}
								setPassword={setPassword}
								msgError={msgError}
								setMsgError={setMsgError}
								handleReturn={handleReturn}
							/>
						</Route>
						<Route path="/signup">
							<SignUp
								msgError={msgError}
								setMsgError={setMsgError}
								handleReturn={handleReturn}
							/>
						</Route>
						<Route path="/account">
							<Account userInfo={userInfo} handleReturn={handleReturn} />
						</Route>
						<Route path="/myFavorites">
							<Favorites
								handleReturn={handleReturn}
								userInfo={userInfo}
								userFav={userFav}
								details={details}
								setDetails={setDetails}
								setIsLoading={setIsLoading}
								setUserFav={setUserFav}
								getUserFav={getUserFav}
								handleClick={handleClick}
								handleDetailsDisplay={handleDetailsDisplay}
							/>
						</Route>
						<Route path="/reset-password">
							<ResetPassword handleReturn={handleReturn}/>
						</Route>
						<Route path="/">
							<Home
								isLoading={isLoading}
								setIsLoading={setIsLoading}
								error={error}
								setError={setError}
								handleReturn={handleReturn}
								handleClick={handleClick}
								setDetails={setDetails}
								details={details}
								userInfo={userInfo}
								getUserInfo={getUserInfo}
								getUserFav={getUserFav}
								booksResult={booksResult}
								setBooksResult={setBooksResult}
								setSearchTerm={setSearchTerm}
								fetchBooks={fetchBooks}
								onInputChange={onInputChange}
							/>
						</Route>
					</Switch>
				</AppContext.Provider>
				<Footer />
			</div>
		</Router>
	);
};

export default App;
