import "./Footer.scss";
const Footer = () => {
	return (
		<footer>
			<div className="footer bg-gradient-purple">
				Made in React.js and Django at OpenCLassRooms - Gaëlle Lefeuvre - 2021
			</div>
		</footer>
	);
};

export default Footer;
