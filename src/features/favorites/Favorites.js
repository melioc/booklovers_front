import React, { useState } from "react";
import BookCard from "../books/BookCard";
import "../books/Books.scss";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import FavoritesBookDetails from "./FavoritesBookDetails";

const Favorites = ({
	userFav,
	setUserFav,
	userInfo,
	getUserFav,
	handleReturn,
	setIsLoading,
}) => {
	const [details, setDetails] = useState(false);

	const handleClick = () => {
		setDetails(true);
	};

	const handleDetailsDisplay = () => {
		getUserFav();
		setDetails(false);
	};

	return (
		<div className="books-list">
			{userFav.map((b, index) => {
				return (
					<Router key={index}>
						{details ? (
							<Route path={`/${b.book.id}`} key={b.book.id}>
								<Link
									to="/"
									onClick={handleDetailsDisplay}
									className="btn-return-list"
								>
									<button>Retour à la liste</button>
								</Link>
								<FavoritesBookDetails
									favId={b.id}
									book={b.book}
									status={b.status}
									userId={b.user}
									setDetails={setDetails}
									userInfo={userInfo}
									userFav={userFav}
									setIsLoading={setIsLoading}
									handleReturn={handleReturn}
									setUserFav={setUserFav}
									handleDetailsDisplay={handleDetailsDisplay}
								/>
							</Route>
						) : (
							<Route path="/">
								<div className="book-display" key={b.book.id}>
									<BookCard book={b.book} handleClick={handleClick} />
								</div>
							</Route>
						)}
					</Router>
				);
			})}
			<Link to="/" className="home-link">
				<button className="btn-home" onClick={handleReturn}>
					Accueil
				</button>
			</Link>
		</div>
	);
};

export default Favorites;
