import React, { useState } from "react";
import "../books/Books.scss";
import "./Favorites.scss";
import { Link } from "react-router-dom";
import BookDetail from "../books/BookDetail";
import axiosFavoritesInstance from "../../conf/api.favorites";
import Cookie from "js-cookie";

const FavoritesBookDetails = ({
	book,
	status,
	favId,
	userId,
	setDetails,
	userInfo,
	handleDetailsDisplay,
}) => {
	const [newStatus, setNewStatus] = useState(status);
	const [statusUpdated, setStatusUpdated] = useState({ value: status });
	const [deleteStatus, setDeleteStatus] = useState(false);

	const handleChange = (e) => {
		setNewStatus(e.target.value);
	};

	const updateFavoriteStatus = () => {
		const token = Cookie.get("token");
		axiosFavoritesInstance
			.put(
				`${favId}`,
				{
					user: userId,
					book: book.id,
					status: newStatus,
				},
				{
					headers: {
						Authorization: `Bearer ${token}`,
					},
				}
			)
			.then((res) => setStatusUpdated({ value: res.data.status }));
		alert(`Enregistrer dans ${newStatus}`);
	};

	const deleteBookToFavorite = () => {
		const token = Cookie.get("token");
		axiosFavoritesInstance.delete(
			`${favId}`,
			{
				headers: {
					Authorization: `Bearer ${token}`,
				},
			},
			{
				user: userId,
				book: book.id,
				status: newStatus,
			}
		);
		alert("Livre supprimé des favoris");
	};

	return (
		<div className="favorites-container">
			{deleteStatus ? (
				<BookDetail book={book} userInfo={userInfo} setDetails={setDetails} />
			) : (
				<section>
					<div className="detail-container">
						<div className="details">
							<div key={book.id} className="book-img">
								<img src={book.image_url} alt={book.title} />
							</div>
							<div className="title">
								<h3>{book.title}</h3>
							</div>
							<div className="author">
								{book.authors.map((author, index) => (
									<p key={index}>{author.author_name}</p>
								))}
							</div>
							<div className="description">
								<p>{book.description}</p>
							</div>
							<div className="status">{statusUpdated.value}</div>
						</div>
						<div className="add-book" key={book.id}>
							<select
								name="options"
								id="fav-options"
								value={newStatus}
								onChange={handleChange}
							>
								<option value="">Modifier l'étagère</option>
								<option value="to read">A lire</option>
								<option value="reading">En cours de lecture</option>
								<option value="read">Lu</option>
							</select>
							<div className="shelf-btn">
								<button type="submit" onClick={updateFavoriteStatus}>
									Modifier l'étagère
								</button>
								<button
									className="delete-btn"
									type="submit"
									onClick={() => {
										deleteBookToFavorite();
										setDeleteStatus(true);
									}}
								>
									Supprimer
								</button>
							</div>
						</div>
					</div>
				</section>
			)}
		</div>
	);
};

export default FavoritesBookDetails;
