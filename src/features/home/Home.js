import React from "react";
import { useHistory } from "react-router-dom";
import BooksList from "../books/BooksList";
import BookSearchForm from "../books/BooksSearchForm";
import "./Home.scss";

/* HomePage component */

const Home = ({
	isLoading,
	setIsLoading,
	details,
	handleClick,
	error,
	setError,
	handleReturn,
	userInfo,
	getUserInfo,
	getUserFav,
	booksResult,
	searchTerm,
	setSearchTerm,
	fetchBooks,
	onInputChange,
}) => {
	const history = useHistory();

	// Submit handler
	const onSubmitHandler = (e) => {
		// Prevent browser refreshing after form submission
		e.preventDefault();

		// Call fetch books async function
		fetchBooks();
		history.push("/bookslist");
		setSearchTerm("");
	};

	return (
		<div className="home-container">
			{isLoading ? (
				<div className="booksResult-container">
					<div className="search">
						<BookSearchForm
							onSubmitHandler={onSubmitHandler}
							onInputChange={onInputChange}
							searchTerm={searchTerm}
							error={error}
							setError={setError}
							setIsLoading={setIsLoading}
						/>
					</div>
					<BooksList
						booksResult={booksResult}
						handleReturn={handleReturn}
						handleClick={handleClick}
						details={details}
						userInfo={userInfo}
						getUserInfo={getUserInfo}
						getUserFav={getUserFav}
					/>
				</div>
			) : (
				<div className="homepage-container">
					<div className="homepage">
						<h2>Bienvenue sur le site dédié aux amoureux des livres!</h2>
					</div>
					<div className="search">
						<BookSearchForm
							onSubmitHandler={onSubmitHandler}
							onInputChange={onInputChange}
							searchTerm={searchTerm}
							error={error}
						/>
					</div>
				</div>
			)}
		</div>
	);
};

export default Home;
