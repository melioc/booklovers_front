import React, {useState} from 'react';
import axiosInstance from "../../../conf/api.users";
import {compareArraysAsSet} from "@testing-library/jest-dom/dist/utils";
import {Link} from "react-router-dom";



const ResetPassword = ({handleReturn}) => {
    const [formData, setFormData] = useState({email:""})

    const changeMail = (e) => {
        setFormData({...setFormData, [e.target.name]: e.target.value})
    }

    const {email} = formData

    const resetPassword = async(e) => {
        try {
            e.preventDefault()
            await axiosInstance.post("password/reset/", {
                email: email
            })
            alert("Un lien de réinitialisation vous a été envoyé.")
        } catch(e){
            console.error(e)
        }
    }

    return(
        <div className="resetContainer">
            <form action="POST" className="reset" onSubmit={resetPassword}>
                <label htmlFor="email" className="email">
                    Email
                </label>
                <input
                    className="email-input"
                    type="email"
                    name="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => changeMail(e)}
                />
                <button type="submit" value="Submit">
                    Envoyer
                </button>
            </form>
            <Link to="/" className="home-link">
                <button className="btn-home" onClick={handleReturn}>
                    Accueil
                </button>
            </Link>
        </div>
    )
}

export default ResetPassword;