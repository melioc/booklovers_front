import React from "react";
import { Link } from "react-router-dom";

import "./Account.scss";

export default function Account(userInfo, { handleReturn }) {
	const displayInfo = (userInfo) => {
		const info = userInfo.userInfo;
		return (
			<div className="account-container" key={info._id}>
				<div className="account-info">
					<h2>Pseudo:</h2>
					<p>{info.username}</p>
					<h2>Email:</h2>
					<p>{info.email}</p>
					<Link to="/" className="home-link">
						<button className="btn-home" onClick={handleReturn}>
							Accueil
						</button>
					</Link>
				</div>
			</div>
		);
	};
	return <>{displayInfo(userInfo)}</>;
}
