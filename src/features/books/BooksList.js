import "./Books.scss";
import React, { useState } from "react";
import { BrowserRouter as Router, Route, Link } from "react-router-dom";
import BookDetail from "./BookDetail";
import BookCard from "./BookCard";

const BooksList = ({ booksResult, handleReturn, userInfo }) => {
	const [details, setDetails] = useState(false);

	const handleDetailsDisplay = () => {
		setDetails(false);
	};

	const handleClick = () => {
		setDetails(true);
	};

	return (
		<div className="books-list">
			{booksResult.map((book, index) => {
				return (
					<Router key={index}>
						{details ? (
							<Route path={`/${book.id}`} key={book.id}>
								<Link
									to="/"
									onClick={handleDetailsDisplay}
									className="btn-return-list"
								>
									<button>Retour à la liste</button>
								</Link>
								<BookDetail
									book={book}
									userInfo={userInfo}
									setDetails={setDetails}
								/>
							</Route>
						) : (
							<div className="book-display" key={book.id}>
								<BookCard book={book} handleClick={handleClick} />
							</div>
						)}
					</Router>
				);
			})}
			<Link to="/" className="home-link">
				<button className="btn-home" onClick={handleReturn}>
					Accueil
				</button>
			</Link>
		</div>
	);
};

export default BooksList;
