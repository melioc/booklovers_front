import "./Books.scss";
import React from "react";
import { Link } from "react-router-dom";

const BookCard = ({ book, handleClick }) => {
	return (
		<div className="booksContainer">
			<div className="book-div" key={book.id}>
				<div className="book-img">
					<img src={book.image_url} alt={book.title} />
				</div>
				<div className="book-info">
					<h3>{book.title}</h3>
					{book.authors.map((author, index) => (
						<p key={index}>{author.author_name}</p>
					))}
				</div>
				<div className="details-btn">
					<Link to={`/${book.id}`} onClick={handleClick}>
						<button>Show Details</button>
					</Link>
				</div>
			</div>
		</div>
	);
};

export default BookCard;
