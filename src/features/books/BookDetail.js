import React, { useState } from "react";
import "./Books.scss";
import axiosFavoritesInstance from "../../conf/api.favorites";
import Cookie from "js-cookie";
import { useAppContext } from "../../libs/contextLib";

const BookDetail = ({ book }) => {
	const { isLogged } = useAppContext();
	const [shelfOption, setShelfOption] = useState("");

	const handleChange = (e) => {
		setShelfOption(e.target.value);
	};

	const addBookToFavorites = async () => {
		const token = Cookie.get("token");
		const bookId = book.id;
		try {
			await axiosFavoritesInstance.post(
				"",

				{
					book: bookId,
					status: shelfOption,
				},
				{
					headers: {
						Authorization: `Bearer ${token}`,
					},
				}
			);
			alert(`Livre enregistré dans ${shelfOption}`);
		} catch (e) {
			console.error(e);
		}
	};

	return (
		<section>
			{isLogged ? (
				<div className="detail-container">
					<div className="details">
						<div key={book.id} className="book-img">
							<img src={book.image_url} alt={book.title} />
						</div>
						<div className="title">
							<h3>{book.title}</h3>
						</div>
						<div className="author">
							{book.authors.map((author, index) => (
								<p key={index}>{author.author_name}</p>
							))}
						</div>
						<div className="description">
							<p>{book.description}</p>
						</div>
					</div>
					<div className="add-book" key={book.id}>
						<label htmlFor="fav-options">Ajouter aux favoris:</label>
						<select name="options" id="fav-options" onChange={handleChange}>
							<option value="">--Catégories--</option>
							<option value="to read">A lire</option>
							<option value="reading">En cours de lecture</option>
							<option value="read">Lu</option>
						</select>
						<button type="submit" onClick={addBookToFavorites}>
							Ajouter à l'étagère
						</button>
					</div>
				</div>
			) : (
				<div className="detail-container">
					<div className="details">
						<div key={book.id} className="book-img">
							<img src={book.image_url} alt={book.title} />
						</div>
						<div className="title">
							<h3>{book.title}</h3>
						</div>
						<div className="author">
							{book.authors.map((author, index) => (
								<p key={index}>{author.author_name}</p>
							))}
						</div>
						<div className="description">
							<p>{book.description}</p>
						</div>
					</div>
				</div>
			)}
		</section>
	);
};

export default BookDetail;
