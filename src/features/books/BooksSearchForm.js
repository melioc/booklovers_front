import React from "react";
import "./BooksSearchForm.scss";
const BookSearchForm = ({
	onSubmitHandler,
	searchTerm,
	onInputChange,
	error,
}) => {
	return (
		<form action="GET" onSubmit={onSubmitHandler}>
			<input
				type="text"
				name="search"
				id="search"
				className="search-input"
				placeholder="Recherche"
				required
				value={searchTerm}
				onChange={onInputChange}
			/>

			<button type="submit" className="btn-submit">
				Recherche
			</button>
			{error && (
				<div style={{ color: "red" }}>
					Some error occurred, while fetching api
				</div>
			)}
		</form>
	);
};

export default BookSearchForm;
