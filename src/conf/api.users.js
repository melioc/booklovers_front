import axios from "axios";

// const axiosInstance = axios.create({
// 	baseURL: "https://api.bookslovers.fr/api/auth/",
// });

const axiosInstance = axios.create({
	baseURL: "http://localhost:8080/api/auth/",
});

export default axiosInstance;
