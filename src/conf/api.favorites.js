import axios from "axios";

const axiosFavoritesInstance = axios.create({
	baseURL: "https://api.bookslovers.fr/favorites/",
});

// const axiosFavoritesInstance = axios.create({
// 	baseURL: "http://localhost:8080/favorites/",
// });

export default axiosFavoritesInstance;
