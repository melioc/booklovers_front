import axios from "axios";

const axiosBooksInstance = axios.create({
	baseURL: "https://api.bookslovers.fr/books/",
	headers: {
		"content-type": "application/json",
	},
});

// const axiosBooksInstance = axios.create({
// 	baseURL: "http://localhost:8080/books/",
// 	headers: {
// 		"content-type": "application/json",
// 	},
// });

export default axiosBooksInstance;
